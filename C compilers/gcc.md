# Compiling GCC from Source Code

## Prerequisites

First of all: You should have a C++ compiler installed!  
There is no way around that.

[Install m4](/dependencies/m4.md)
[Install_gmp]

## Install

### gcc 2.95.3

```sh

wget http://ftp.gnu.org/gnu/gcc/gcc-2.95.3/gcc-everything-2.95.3.tar.gz || wget https://mirrors.huaweicloud.com/gnu/gcc/gcc-2.95.3.tar.gz &&
(tar xvf gcc-everything-2.95.3.tar.gz && cd gcc-2.95.3 )|| (tar xvf gcc-2.95.tar.gz && cd gcc-2.95.3) &&

```
