# Compile OpenSSL from Source Code

## Compile, Test and Install

```sh
mkdir -p ~/sources &&
cd ~/sources &&
wget https://github.com/openssl/openssl/archive/refs/heads/master.zip &&
unzip master.zip &&
rm master.zip &&
cd openssl-master &&
CFLAGS="-O3" CXXFLAGS="-O3" \
./Configure \
    --prefix=$HOME/softwares/openssl \
    --openssldir=$HOME/ssl &&
make -j$(nproc) && make -j$(nproc) test && make install &&
echo '# OpenSSL
export PATH=$PATH:$HOME/softwares/openssl/bin
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$HOME/softwares/openssl/lib64/pkgconfig
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/softwares/openssl/lib64
export CPPFLAGS="$CPPFLAGS -I $HOME/softwares/openssl/include"' >> ~/.myPATH
```
