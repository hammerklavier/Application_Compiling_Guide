# Compile git from Source Code

## Prerequisites

[OpenSSL](openssl.md)
[curl](curl.md)
[expat](/dependencies/libexpat.md)

## Install

```bash
mkdir -p ~/sources &&
cd ~/sources &&
wget https://github.com/git/git/archive/refs/heads/master.zip &&
unzip master.zip &&
rm master.zip &&
cd git-master/ &&
make prefix=$HOME/softwares/git all doc info LDFLAGS="-L/home/jimmy/softwares/openssl/lib64" CPPFLAGS="-I/home/jimmy/softwares/openssl/include"
make prefix=$HOME/softwares/git install install-doc install-html install-info 

```
