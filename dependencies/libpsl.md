# Compile libpsl from Source

## Install

```sh
mkdir -p ~/sources &&
cd ~/sources &&
wget https://github.com/rockdaboot/libpsl/releases/download/0.21.5/libpsl-0.21.5.tar.gz &&
tar xvf libpsl-0.21.5.tar.gz &&
cd libpsl-0.21.5 &&
mkdir -p build && cd build &&
CFLAGS=-O3 ../configure \
    --prefix=$HOME/softwares/libpsl-0.21.5 &&
make -j$(nproc) && make -j$(nproc) check && make install &&
echo '
# libpsl-0.21.5
export PATH=$PATH:$HOME/softwares/libpsl-0.21.5/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/softwares/libpsl-0.21.5/lib
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$HOME/softwares/libpsl-0.21.5/lib/pkgconfig' >> ~/.myPATH
