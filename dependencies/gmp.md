# Compile gmp from Source Code

## Install

```sh
cd ~/sources && 
wget https://mirrors.huaweicloud.com/gnu/gmp/gmp-6.3.0.tar.xz && 
tar xvf gmp-6.3.0.tar.xz && cd gmp-6.3.0 && 
mkdir -p bld && cd bld && 
CFLAGS="-O3" CXXFLAGS="-O3" \
../configure --prefix=$HOME/softwares/gmp-6.3.0 \
    --enable-shared \
&& 
make -j$(nproc) && make -j$(nproc) check && make install &&
echo "# gmp-6.3.0" >> ~/.myPATH &&
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/softwares/gmp-6.3.0/lib' >> ~/.myPATH &&
```
