# compile mpc from source code

## Dependencies

[gmp](gmp.md)
[mpfr](mprf.md)

## Install

```sh
cd ~/sources && 
wget https://mirrors.huaweicloud.com/gnu/mpc/mpc-1.3.1.tar.gz && 
tar xvf mpc-1.3.1.tar.gz && cd mpc-1.3.1 && 
mkdir -p bld && cd bld && 
CFLAGS="-O3" CXXFLAGS="-O3" \
../configure --prefix=$HOME/softwares/mpc-1.3.1 \
    --with-gmp=$HOME/softwares/gmp-6.3.0 \
    --with-mpfr=$HOME/softwares/mpfr-4.2.1 \
    && 
make -j$(nproc) && make -j$(nproc) check && make install &&
echo '# mpc-1.3.1' >> ~/.myPATH
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/softwares/mpc-1.3.1/lib' >> ~/.myPATH
``
