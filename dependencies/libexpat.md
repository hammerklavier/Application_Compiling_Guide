# Compile libexpat from source code

## Dependencies

- C++ 11 compiler
  - [gcc](/C%20compilers/gcc.md)

## Install

```sh
cd ~/sources &&
wget https://github.com/libexpat/libexpat/releases/download/R_2_6_2/expat-2.6.2.tar.xz &&
tar xvf expat-2.6.2.tar.xz && cd expat-2.6.2 &&
mkdir -p build && cd build &&
CFLAGS=-O3 \
../configure --prefix=$HOME/softwares/expat-2.6.2 &&
make -j$(nproc) && make check -j$(nproc) && make install
```
