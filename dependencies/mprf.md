# Compile mprf from Source Code

## Install

```sh
cd ~/sources && 
wget https://mirrors.huaweicloud.com/gnu/mpfr/mpfr-4.2.1.tar.xz && 
tar xvf mpfr-4.2.1.tar.xz && cd mpfr-4.2.1 && 
mkdir -p bld && cd bld && 
CFLAGS="-O3" CXXFLAGS="-O3" \
../configure --prefix=$HOME/softwares/mpfr-4.2.1 \
    --with-gmp=$HOME/softwares/gmp-6.3.0 \
    --enable-thread-safe && 
make -j$(nproc) && 
make -j$(nproc) check && make -j$(nproc) check-gmp-symbols && 
make install &&
echo '# mpfr-4.2.1' >> ~/.myPATH &&
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/softwares/mpfr-4.2.1/lib' >> ~/.myPATH &&
echo 'export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$HOME/softwares/mpfr-4.2.1/lib/pkgconfig' >> ~/.myPATH
```
