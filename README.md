# Application_Compiling_Guide

This is a guide for compiling frequently used application programmes / softwares from source code, currently on Linux, aiming at relying nothing from repository.

这个项目是源码编译常见应用程序的教程，旨在不依赖任何软件仓库提供的任何程序/库。目前仅提供 Linux 端的教程。

## Contributors

hammerklavier

## Testing Environment

- Huawei Cloud EulerOS 2.0 标准版 64位
- Ubuntu 22.04 LTS （将被测试）

## Primitive Requirements

```sh
echo "
# myPATH
source ~/.myPATH" >> ~/.bashrc
```

[git](web/git.md)
